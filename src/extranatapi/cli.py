# -*- coding: utf-8 -*-
"""Extranat."""


from extranatapi import Extranatcli


def main():
    """Run main process."""

    extranatcli = Extranatcli()
    extranatcli.run()


if __name__ == "__main__":

    main()
