# Extranat API - changelog

```txt
0.0.0.7
  - Prise en compte de la mise à joru des sites Web Extranat pour les nageurs/compétitions
  - Restriction: ne fonctionne pas pour récupérer la liste des régions/structures/départements

0.0.0.6
  - Ajout de "date de nage" pour l'extraction des nages d'un nageur.

0.0.0.5
  - Documentation
  - Ajout de la fonctionnalité "Liste des nages": --list-nages

0.0.0.4
  - Module pip

0.0.0.2
  - Ajout de la fonctionnalité "Recherche d'une équipe": --recherche_equipe

0.0.0.1
  - Création
```
